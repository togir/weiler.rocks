---
title: "Vegane Butter"
lang: "de"
date: 2024-10-19T00:00:00Z
summary: "Rezept zum Herstellen von veganer Butter. Dauer 10 Minuten."
tags: ["Rezept"]
draft: false
---

_1:10 Stunden (davon 1 Stunde Ruhezeit)_

== Zutaten
- 100g Kokosöl,
- 80g Flüssiges Öl
- 50 ml Hafermilch
- Kurkuma (für die Farbe)
- Salz

Hafermilch, Kurkuma und Öl in einen Behälter geben und mischen.
Dann das Kokosöl hinzugeben. Mit einem Mixer 1-2 MInuten pürieren.

Das ganze eine Stunde kühl stellen.
Nun noch einmal mit dem Mixer pürieren, da sich das Kurkuma meistens absetzt.

Anschließend in das Aufbewahrungsgefäß umfüllen.


