---
title: "Using Katex with Restructured Text"
date: 2020-01-06T18:55:47+01:00
tags: ["katex", "restructuredtext", "latex"]
draft: false
summary: "Katex is a LaTeX in HTML renderer. But there can be some incompatibilities with the Restructured Text standard."
---

Problem:
.........

Including Katex into a Hugo site? That's easy! Just follow the instructions at https://katex.org/docs/autorender.html and add the Autoloader script-tags to your footer.

But on posts that are using the .rst format the rendering was broken. For example the Latex equation **"\\forall x,y \\in \\mathbb{N}"** should be rendered as

.. raw:: html

    $$ \forall x,y \in \mathbb{N} $$

but instead there was $$ forall x, y in mathbb{N} $$

The issue can be found in the processing of the .rst file.  Because "\\" is the escape-character in Restructured Text all "\\" are stripped off from the html-output.

Solution:
.........

You could just escape the "\\" with another "\\" so that the equation will look like this **"\\\\forall x,y \\\\in \\\\mathbb{N}"**. This might work for small formulas but it's not perfect. Because now we have to type much more "\\".

The more convenient solution is to use a *Directive* named "raw". With **".. raw:: html""** the Restructured Text-Parser will bypass a section of the content so that Katex can do the rendering. It can be utilized like this:

.. raw:: html

    <pre><code class="language-markup">
        Some content above.

        .. raw:: html

            $$ \forall x,y \in \mathbb{N} $$

        Normal content below the ignored section.
        The blank lines around .. raw:: html are important as it is the indetation!
    </code></pre>

More information about this Directive can be found at the `Docutils referenze <https://docutils.sourceforge.io/0.4/docs/ref/rst/directives.html#raw-data-pass-through>`_
