---
title: "Vegane Maultaschen mit Spinat"
lang: "de"
date: 2024-10-20T18:10:00Z
summary: "Rezept fuer Vegane Maultaschen mit Spinat. Zeitaufwand ca. 2:20 Stunden, davon 2 Stunden Ruhezeit."
tags: ["rezept"]
draft: false
---

_2:20 Stunden (davon 2 Stunden Ruhezeit), ein Topf, Pfanne und Mixer._

== Zutaten

Für den Nudelteig:
- 200 g Hartweizengrieß
- 90 ml Wasser
- 1 EL Öl 
- Salz 

Für die Füllung:
- 120 g Spinat 
- 1 Zwiebel
- 1 Knoblauchzehe
- 160 g Räuchertofu
- 2 EL Petersilie
- 1 TL Stärke
 -1 EL Sojasauce
- 50 g Semmelbrösel
- 2 EL Bratöl
-Salz und Pfeffer

== Anleitungen

Für den Teig:
Für den Pastateig 200 g Grieß, 90 ml Wasser, 1 EL Öl und Salz in eine Schüssel geben. Den Teig 5 Minuten kneten. Es soll ein fester Teig entstehen. 

Den Pastateig luftdicht einpacken(z.B. in eine Dose) und 2 Stunden bei Zimmertemperatur ruhen lassen.

Für die Füllung:
Räuchertofu bröseln. Zwiebeln und Tofu in der Pfanne anbraten. Knoblauch und Spinat dazugeben.

Dann die gebratenen Zutaten mit allen weiteren Zutaten der Füllung (2 EL Petersilie fein geschnitten, 1 TL Stärke, 1 EL Sojasauce, 50 g Semmelbrösel) pürieren. Jetzt mit Salz und Pfeffer würzen.
Für die Maultaschen

Den Nudelteig nochmal kurz durchkneten und dann auf einer mit Mehl bestreuten Arbeitsplatte dünn (weniger als 2 mm) ausrollen und lange rechteckige Teigbahnen zurechtschneiden.

Die Maultaschenfüllung vom unteren Rand auf dem Nudelteig ca. einen halben Zentimeter dick verstreichen. Oben und unten eine Rand lassen.

Wenn die Füllung gleichmäßig verteilt wurde, die freien Teigränder mit etwas Wasser benetzen und nun von unten ca. ein Drittel der Länge nach umklappen und etwas andrücken.

Anschließend noch ein letztes mal umklappen und das Ganze einmal wenden. Die Nahtstelle gleichmäßig vorsichtig andrücken und glatt streichen.

Teig in Maultaschenform drücken, und kleinschneiden.

Die Maultaschen für 2-4 Minuten in Wasser garkochen.
