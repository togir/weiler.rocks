---
title: "Zucchini-Brownies"
lang: "de"
date: 2024-10-20T20:37:00Z
summary: "Rezept für vegane Zucchini-Brownies mit Schokostücken"

tags: ["rezept"]
draft: false
---

_60 Minuten, ein Blech und Ofen._

== Zutaten

- 600 g Zucchini geraspelt 
- 2 Bananen
- 360 g Mehl
- 180 g Kakaopulver
- 2 TL Backpulver oder 
- etwas Salz 
- 150 g Öl
- 200ml Hafermilch
- viel Schokolade ;)


== Anleitung

Die Zucchini raspeln, und die Bananen matschen.
Alles vermischen und kneten. 

40 Minuten bi 180°C in den Ofen.
Noch warm oben mit Schokolade bestreuen.
